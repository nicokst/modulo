package io.noko.module;

/**
 * 
 * @author nicokst (https://nicokst.de/)
 * ---
 * Die Klasse welche vom "Modul" geerbt wird, 
 * dies kann (und wird 50%ig) noch weiter ausgebaut werden
 *
 */
public abstract class Module {

	private ModuleDescription desc;

	public ModuleDescription getDescription() {
		return desc;
	}

	protected void setDescription(ModuleDescription desc) {
		this.desc = desc;
	}

	public void onEnable() {
		System.out.println("Enabling " + getDescription().getName() + " v" + getDescription().getVersion() + " by "
				+ getDescription().getAuthors().toString());
	}

}
