package io.noko.module;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * @author nicokst (https://nicokst.de/)
 * ----
 * Diese Klasse ist sozusage das Herz, 
 * hier werden die Dateien gelistet, gepr�ft, und im Endeffekt 
 * werden die Module aktiviert.
 */
public class ModuleManager {

	private static ModuleManager instance = null;

	private Map<File, ModuleDescription> possible_modules;
	private Map<Module, ModuleDescription> initialized;

	private ModuleManager() {
		this.initialized = new HashMap<Module, ModuleDescription>();
		this.possible_modules = new HashMap<File, ModuleDescription>();
	}

	public static ModuleManager getInstance() {
		if (ModuleManager.instance == null) {
			ModuleManager.instance = new ModuleManager();
		}
		return ModuleManager.instance;
	}

	public void findModules() {
		FileUtils.listFiles(new File("modules/"), new String[] { "jar" }, false).forEach(f -> {
			try {
				JarFile jar = new JarFile(f);
				JarEntry desc = jar.getJarEntry("desc.json");
				if (desc == null) {
					System.out.println("Modulo >> Cannot enable " + f.getName() + " -> No desc.json");
				} else {
					JSONParser parser = new JSONParser();
					InputStream in = jar.getInputStream(desc);
					JSONObject pluginDescription = (JSONObject) parser.parse(new InputStreamReader(in));
					ModuleDescription description = new ModuleDescription(pluginDescription);
					if (description.isValid()) {
						possible_modules.put(f, description);
					} else {
						System.out.println("Modulo >> '" + f.getName()
								+ "' doesn't have a valid desc.json, does it contain all fields?");
					}
				}
				jar.close();
			} catch (Exception e) {
			}
		});
		System.out.println("Modulo >> Found " + possible_modules.size() + " possible module(s)");
	}

	public void initModules() {
		possible_modules.keySet().forEach(file -> {
			ModuleDescription description = possible_modules.get(file);
			String main = description.getMainClass();
			try {
				URLClassLoader classLoader = new URLClassLoader(new URL[] { file.toURI().toURL() });
				Class<? extends Module> moduleClass = (Class<? extends Module>) classLoader.loadClass(main);
				Module module = (Module) moduleClass.newInstance();
				module.setDescription(description);
				module.onEnable();
				initialized.put(module, description);
				classLoader.close();
			} catch (ClassNotFoundException | InstantiationException
					| IllegalAccessException | IOException e) {
				e.printStackTrace();
			}
		});
	}

}
