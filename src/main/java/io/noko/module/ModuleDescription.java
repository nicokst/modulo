package io.noko.module;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * @author nicokst (https://nicokst.de/)
 * ---
 * Diese Datei ist eine bessere implementation des JSON-Objekts f�r die Beschreibung (desc.json)
 */
public class ModuleDescription {

	private JSONObject object;
	
	public ModuleDescription(JSONObject object) {
		this.object = object;
	}
	
	public JSONObject getObject() {
		return object;
	}
	
	public boolean isValid() {
		return object.containsKey("name") && object.containsKey("description") && object.containsKey("main") && object.containsKey("authors") && object.containsKey("version");
	}
	
	public String getName() {
		return (String) object.get("name");
	}
	
	public String getDescription() {
		return (String) object.get("description");
	}
	
	public String getVersion() {
		return (String) object.get("version");
	}
	
	public String getMainClass() {
		return (String) object.get("main");
	}
	
	public List<String> getAuthors() {
		List<String> out = new ArrayList<>();
		JSONArray array = (JSONArray) object.get("authors");
		for(Object o : array) {
			out.add((String) o);
		}
		return out;
	}
	
	@Override
	public String toString() {
		return object.toJSONString();
	}
	
}
