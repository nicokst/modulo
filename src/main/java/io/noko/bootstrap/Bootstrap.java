package io.noko.bootstrap;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import io.noko.module.ModuleManager;

/**
 * @author nicokst (https://nicokst.de/)
 * ---
 * Bootstrap Klasse, f�hrt in diesem Fall nur die Test-Module aus
 */
public class Bootstrap {

	public static void main(String[] args) throws IOException {
		FileUtils.forceMkdir(new File("modules"));
		ModuleManager manager = ModuleManager.getInstance();
		manager.findModules();
		manager.initModules();
	}
	
}
